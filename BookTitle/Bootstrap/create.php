<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../../Asset/css/bootstrap.min.css">
  <script src="../../Asset/js/jquery.min.js"></script>
  <script src="../../Asset/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <div class="col-sm-4 col-sm-offset-4">
  <form class="form-horizontal">
    <div class="form-group" >
      <label for="title">Book Title:</label>
        <input type="text" class="form-control" id="booktitle" name="title">
    </div>
    <div class="form-group" >
      <label for="author">Book Author:</label>
        <input type="text" class="form-control" id="booktitle" name="author">
    </div>

    <div class="form-group">
      <button type="submit" name="submit" class="btn btn-success col-sm-4 glyphicon glyphicon-book">  Edit</button>
    </div>
  </form>
</div>
</div>
</body>
</html>